
import React, { Component } from 'react';
import {
    StyleSheet,
    ListView,
    TouchableOpacity
} from 'react-native';

// import self files
import ArtistBox from './ArtistBox';
import {Actions} from 'react-native-router-flux';

export default class ArtistList extends Component {
	constructor(props){
        super(props);
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds
        }
    }
    componentDidMount() {
        this.updateDataSource(this.props.artists);
    }

    // cada vez que se cambian las propiedades que recibe el componente
    componentWillReceiveProps(newProps) {
        if(newProps.artists !== this.props.artists){
            this.updateDataSource(newProps.artists);
        }
    }
	render() {
        return (
            <ListView 
                enableEmptySections={true}
                dataSource={this.state.dataSource}
                renderRow={(artist) => {
                        return(
                            <TouchableOpacity onPress={() => this.handlePress(artist)}>
                                <ArtistBox artist={artist} />
                            </TouchableOpacity>
                        );
                    }}
            />
		);
    }
    handlePress(artist){
        Actions.artistDetail({artist});
    }
    updateDataSource(data){
        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(data)
        });
    }
}

const styles = StyleSheet.create({
  	container: {
		flex: 1,
        backgroundColor: 'lightgray',
        paddingTop: 30
    },
});
