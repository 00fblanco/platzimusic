
import React, { Component } from 'react';
import {
	StyleSheet,
    View,
    ActivityIndicator,
    Platform
} from 'react-native';

// import self files
import ArtistList from './ArtistList';
import { getArtists } from './APIClient';

export default class HomeView extends Component {
    state = {
        artists: null
    }
    componentDidMount(){
        getArtists().then(data => this.setState({ artists: data}))
    }

	render() {
        const artists = this.state.artists;
        return (
            <View style={styles.container} >
                {!artists && <ActivityIndicator size="large"/>}
                {artists && <ArtistList artists={artists}/>}
            </View>
		);
  	}
}

const styles = StyleSheet.create({
  	container: {
		flex: 1,
        backgroundColor: '#e2e2e3',

        paddingTop: Platform.select({
            ios: 30,
            android: 10
        })
    },
});