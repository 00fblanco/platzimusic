
import React, { Component } from 'react';
import {
	StyleSheet,
    View,
    TextInput,
    TouchableOpacity,
    Text
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
// import self files
import ArtistBox from './ArtistBox';
import CommentList from './CommentList';
import { getArtists } from './APIClient';
import {firebaseDatabase, firebaseAuth} from './firebase';

export default class ArtistDetailView extends Component {
    state = {
        comments: []
    }
    addComment = (data) => {
        const comment = data.val();
        this.setState({
            comments: this.state.comments.concat(comment)
        })
    }

    componentDidMount() {
        this.getArtistCommentsRef().on('child_added', this.addComment);
    }
    componentWillUnmount() {
        this.getArtistCommentsRef().off('child_added', this.addComment);
        
    }
    handleChangeText = (text) => this.setState({text})
    handleSend = () => {
        //console.warn('enviar', this.state.text)
        const {text} = this.state
        const {uid, photoURL} = firebaseAuth().currentUser
        const artistCommentsRef = this.getArtistCommentsRef()
        var newComment = artistCommentsRef.push();
        newComment.set({
            text,
            userPhoto: photoURL,
            uid
        });
        this.setState({
            text: ''
        })
    }

    getArtistCommentsRef = () => {
        const {id} = this.props.artist
        return firebaseDatabase.ref(`comments/${id}`);

    }
    render() {
        const artist = this.props.artist;
        const {comments} = this.state
        return (
            <View style={styles.container} >
                <ArtistBox artist={artist}/>
                <CommentList comments={comments}/>
                <View style={styles.inputContainer}>
                    <TextInput
                        value={this.state.text}
                        style={styles.input}
                        placeholder="Opina sobre este artista"
                        onChangeText={this.handleChangeText}

                    />
                    <TouchableOpacity onPress={this.handleSend}>
                        <Icon size={30} name="ios-send-outline" color="gray"/>
                    </TouchableOpacity>
                </View>
            </View>
		);
  	}
}

const styles = StyleSheet.create({
  	container: {
		flex: 1,
        backgroundColor: '#e2e2e3',
        paddingTop: 10,
        
    },
    header: {
       fontSize: 20,
       paddingHorizontal: 15,
       marginVertical: 10   
    },
    inputContainer: {
        flexDirection: 'row',
        height: 50,
        backgroundColor: 'white',
        paddingHorizontal: 10,
        alignItems: 'center'
    },
    input: {
        flex: 1,
        height: 50
    }
});