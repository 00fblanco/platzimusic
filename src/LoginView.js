
import React, { Component, PropTypes } from 'react';
import {
	StyleSheet,
    View,
    Text,
    Button,
    Image,
} from 'react-native';

import FBSDK, {LoginButton, AccessToken} from 'react-native-fbsdk';
import {Actions} from 'react-native-router-flux';
import firebase, {firebaseAuth, firebaseDatabase} from './firebase';

const {FacebookAuthProvider} = firebase.auth;


export default class LoginView extends Component {
    state = {
        credentials: {}
    }
    componentDidMount(){
        this.authenticateUser();
    }
    authenticateUser = () => {
        AccessToken.getCurrentAccessToken()
        .then(data => {
            const {accessToken} = data;
            const credential = FacebookAuthProvider.credential(accessToken);
            firebaseAuth().signInWithCredential(credential)
                .then((credentials) => {
                    console.log("Sign in success", credentials);
                    this.setState({credentials})
                    Actions.home();
                }, function(error){
                    console.log("Sign in error", error);
                })
        });
    }
    handleLoginFinished = (error, result) => {
        if(error){
            console.error(error);
        } else if(result.isCancelled){
            alert("Login is cancelled.")
        }else{
            
            //alert(data.accessToken.toString());
            // Actions.home();
            this.authenticateUser();
        }
    }
	render() {
        return (
            <Image source={require('./background.jpg')} style={styles.container}>
                <Image source={require('./logo.png')} style={styles.logo} />
                <Text style={styles.welcome}> Bienvenido a Platzi Music </Text>
                <Text style={styles.welcome}>
                    {this.state.credentials && this.state.credentials.displayName}
                </Text>
                 <LoginButton
                    readPermissions={["public_profile", "email"]}
                    onLoginFinished={this.handleLoginFinished}
                    onLogoutFinished={
                        () => alert("Logout.")
                    }
                /> 
            </Image>
		);
  	}
}
const styles = StyleSheet.create({
  	container: {
        flex: 1,
        width: null,
        height: null,
        backgroundColor: '#e2e2e3',
        justifyContent: 'center',
        alignItems: 'center'
    },
    welcome: {
        fontSize: 24,
        fontWeight: '600',
        marginBottom: 20,
        backgroundColor: 'transparent',
        color: 'white'
    },
    logo: {
        width: 150,
        height: 150,
        marginBottom: 15
    }
});