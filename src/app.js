
import React, { Component } from 'react';
import {
	AppRegistry,
	StyleSheet,
    View,
    Platform
} from 'react-native';

import {Scene, Router} from 'react-native-router-flux';

// import self components
import HomeView from './HomeView';
import ArtistDetailView from './ArtistDetailView';
import LoginView from './LoginView';

class PlatziMusic extends React.Component{
    
    render(){
        //const isAndroid = Platform.OS === 'android';

        return(
            <Router>
                <Scene key="root">
                    <Scene key="login" component={LoginView} hideNavBar={true} />
                    <Scene key="home" component={HomeView} hideNavBar={true}/>
                    <Scene key="artistDetail" component={ArtistDetailView} title="Comentarios" hideNavBar={false} />
                </Scene>
            </Router>   
        );
    }
}

AppRegistry.registerComponent('PlatziMusic', () => PlatziMusic);
