import React from 'react';
import {
    Text,
    StyleSheet,
    View,
    Image
} from 'react-native'

const DEFAULT_AVATAR = 'https://wk3.org/assets/user/default-b7156fd890e4b339a5b41e0bf8aa6932.png'
const AVATAR_SIZE = 32
const Comment = (props) => 
    <View style={styles.comment}>
        {
            props.avatar ?
                <Image style={styles.avatar} source={{uri: props.avatar}} />:
                <Image style={styles.avatar} source={{uri: DEFAULT_AVATAR}} />
        }
        <Text style={styles.text}>{props.text} </Text>
    </View>

export default Comment;

const styles = StyleSheet.create({
    comment: {
        backgroundColor: '#ecf0f1',
        padding: 10,
        margin: 5,
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'center'
    },
    text: {
        fontSize: 16,
        marginLeft: 10
    },
    avatar: {
        width: AVATAR_SIZE,
        height: AVATAR_SIZE,
        borderRadius: AVATAR_SIZE / 2
    }
})