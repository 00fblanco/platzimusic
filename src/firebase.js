import * as firebase from 'firebase';

const config = {
    apiKey: "AIzaSyCp_PaPAe3qof0rPWXPIBB2NUlowK9tUMo",
    authDomain: "platzimusic-891ca.firebaseapp.com",
    databaseURL: "https://platzimusic-891ca.firebaseio.com",
    projectId: "platzimusic-891ca",
    storageBucket: "platzimusic-891ca.appspot.com",
    messagingSenderId: "242332977057"
  };
  firebase.initializeApp(config);
  export const firebaseAuth = firebase.auth;
  export const firebaseDatabase = firebase.database();
  export default firebase;
  